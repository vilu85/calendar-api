## Assignment 11.9: Deploy Calendar API via CI/CD

Create a Gitlab repository for the Calendar API. Enable continuous development in the Azure Web App. Create a CI/CD pipeline that builds and deploys the Calendar API.

Calendar API comes with a baked in default event which can be found by using the event id **1** or by requesting events from june (GET request  `https://<api-url>/6`)

- [Endpoints](#endpoints)
  - [Endpoint: Return a list of all events](#endpoint-return-a-list-of-all-events)
    - [Method: GET](#method-get)
  - [Endpoint: Add the new event](#endpoint-add-the-new-event)
    - [Method: POST](#method-post)
    - [Headers](#headers)
    - [Body (**raw**)](#body-raw)
  - [Endpoint: Return the event by month](#endpoint-return-the-event-by-month)
    - [Method: GET](#method-get-1)
  - [Endpoint: Modify the event by id](#endpoint-modify-the-event-by-id)
    - [Method: PUT](#method-put)
    - [Headers](#headers-1)
    - [Body (**raw**)](#body-raw-1)
  - [Endpoint: Delete the event by id](#endpoint-delete-the-event-by-id)
    - [Method: DELETE](#method-delete)
    - [Headers](#headers-2)
    - [Body (**raw**)](#body-raw-2)

# Endpoints

## Endpoint: Return a list of all events
### Method: GET
>```
>http://localhost:3000/
>```

## Endpoint: Add the new event
Adds an event to the calendar
### Method: POST
>```
>http://localhost:3000/
>```
### Headers

|Content-Type|Value|
|---|---|
|Content-Type|application/json|

### Body (**raw**)

```json
{
    "title": "Test title",
    "description": "Test description",
    "date": "2023-05-12"
}

```

## Endpoint: Return the event by month
Returns a list of all the events in the next month given as parameter
### Method: GET
>```
>http://localhost:3000/{{monthNumber}}
>```

## Endpoint: Modify the event by id
Adds an event to the calendar
### Method: PUT
>```
>http://localhost:3000/{{eventId}}
>```
### Headers

|Content-Type|Value|
|---|---|
|Content-Type|application/json|

### Body (**raw**)

```json
{
    "title": "Changed title"
}

```

## Endpoint: Delete the event by id
Adds an event to the calendar
### Method: DELETE
>```
>http://localhost:3000/{{eventId}}
>```
### Headers

|Content-Type|Value|
|---|---|
|Content-Type|application/json|

### Body (**raw**)

```json

```
