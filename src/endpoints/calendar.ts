import express, { type Request, type Response } from 'express';

interface Event {
    id: number;
    title: string;
    description: string;
    date: string;
    time?: string;
}

let events: Event[] = [
    {
        id: 1,
        date: "2023-06-06",
        title: "Pipeline test title",
        description: "Testing a default event baked in the docker container",
        time: "18:46:00"
    }
];
let ids = events.length;

const calendarRouter = express.Router();

// Endpoint for returning all events
calendarRouter.get('/', (_request: Request, response: Response): void => {
    response.status(200).json(events);
});

// Endpoint for creating a new event
calendarRouter.post('/', (request: Request, response: Response): void => {
    const { date, description, title, time } = request?.body as Event;

    // Create a new id for the event
    const newEvent: Event = { date, description, title, time, id: ++ids };
    events.push(newEvent);
    response.status(201).json(newEvent);
});

// Endpoint for deleting the event
calendarRouter.delete('/:eventId', (request: Request, response: Response): void => {
    const id = request?.params?.eventId;
    const oldLength = events.length;
    events = events.filter((event) => event.id !== Number(id));

    if (events.length < oldLength) {
        response.status(204).send();
    } else {
        response.status(404).json({ error: `Event id "${id}" does not exist.` });
    }
});

// Endpoint for modifying the event
calendarRouter.put('/:eventId', (request: Request, response: Response): void => {
    const id = request?.params?.eventId;
    const updatedEvent: Event = request.body as Event;

    let wasModified = false;

    events = events.map((event) => {
        if (event.id === Number(id)) {
            wasModified = true;
            return { ...event, ...updatedEvent };
        }
        return event;
    });

    if (wasModified) {
        response.status(200).send();
    } else {
        response.status(404).json({ error: `Event id "${id}" does not exist.` });
    }
});

// Endpoint for returning events by month
calendarRouter.get('/:monthNumber', (request: Request, response: Response): void => {
    const monthNumber = Number(request.params.monthNumber);

    const result = events.filter((event) => {
        const date = new Date(event.date);
        const month = date.getMonth();
        return month + 1 === monthNumber;
    });

    response.status(200).json(result);
});

export default calendarRouter;
