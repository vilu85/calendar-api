/**
 * ## Assignment 11.9: Deploy Calendar API via CI/CD
 * 
 * Create a Gitlab repository for the Calendar API. Enable continuous development in the Azure Web App. Create a CI/CD
 * pipeline that builds and deploys the Calendar API.
 */
import server from './server';
const PORT = 3000;

// Starts listening for the port
server.listen(PORT, () => {
    console.log(`Listening to port ${PORT}`);
});
