import express, { type Express, type Request, type Response, type NextFunction } from 'express';

/**
 * Sets all middlewares for the server.
 * @param server Express server instance
 */
export const initMiddlewares = (server: Express): void => {
    // Setup JSON parser middleware
    server.use(express.json());

    // JSON parse error handler
    server.use(handleJSONParseError);

    // Logger middleware
    server.use(logger);
};

/**
 * Middleware that logs time, method and request body to the console log.
 * @param req Request
 * @param _res Response
 * @param next Next function callback
 */
export const logger = (req: Request, _res: Response, next: NextFunction): void => {
    const date = new Date();
    const padNumbers = (value: string | number): string => String(value).padStart(2, '0');
    const dateStr = `${padNumbers(date.getHours())}:${padNumbers(date.getMinutes())}:${padNumbers(date.getSeconds())}`;
    const method = req.method;
    const host = req.headers.host ?? '';
    const url = new URL(req.url, `http://${host}`);
    const body = req.body as JSON;
    console.log(`${dateStr} ${method}\t${url.pathname}\t`, body);
    next();
};

/**
 * Middleware for handling unknown end points
 * @param req request
 * @param res response
 */
export const unknownEndpoint = (req: Request, res: Response): void => {
    const host = req.headers.host ?? '';
    const url = new URL(req.url, `http://${host}`);
    res.status(404).json({ error: `Endpoint ${url.pathname} does not exist` });
};

/**
 * Middleware for handling JSON parsing errors
 * @param err Error object
 * @param req request
 * @param res response
 * @param next next callback
 */
export function handleJSONParseError(err: Error, req: Request, res: Response, next: NextFunction): void {
    if (err instanceof SyntaxError && err.message.includes('JSON')) {
        res.status(400).json({ error: 'Invalid JSON' });
    } else {
        next(err);
    }
}
