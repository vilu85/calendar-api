import express from 'express';
import { initMiddlewares, unknownEndpoint } from './middlewares';
import calendarRouter from './endpoints/calendar';

const server = express();

const apiPath = '/';

// Sets middlewares for the server
initMiddlewares(server);

// Setup routers
server.use(`${apiPath}`, calendarRouter);

// Unknown endpoint handler
server.use(unknownEndpoint);

export default server;
